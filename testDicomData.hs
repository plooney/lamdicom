import DicomData
import qualified Data.ByteString.Lazy as BS

main :: IO()
main =  do contents <- BS.readFile("/home/padraig/Ubuntu One/rccCnr") 
           print $  take 100 $ getDicomDataSet $ readMainDataSet contents
           print $ "\n Meta Set \n"
           print $ getDicomDataSet $ readFileMetaDataSet contents
