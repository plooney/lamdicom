import Text.Regex.TDFA
import System.Environment
import System.Exit
import Control.Monad

main :: IO() 
main = getArgs >>= parse >>=  putStr . unlines . lines 

parse :: [String] -> IO String 
parse ["-h"] = usage   >> exit
parse ["-v"] = version >> exit
parse []     = getContents
parse [a, b] = findVals a b
--parse fs     = concat `fmap` mapM readFile fs

findVals :: String -> String -> IO String
--findVals a b = readFile >>= (\x -> return (x)) $ b 
findVals a b = show matches
    where fileContents = readFile b 
          matches = (liftM .  isString) a fileContents

isString :: String -> String -> [[String]]
isString a b = b =~ a :: [[String]]

usage   = putStrLn "Usage: tac [-vh] [file ..]"
version = putStrLn "Haskell tac 0.1"
exit    = exitWith ExitSuccess
die     = exitWith (ExitFailure 1)


