module DicomData (DicomElem,
Value,
Group,
Element,
fileMetaInfo,
readFileMeta,
readMainDataSet,
readFileMetaDataSet,
getDicomDataSet
) where

import qualified Data.ByteString.Lazy as BS
import qualified Data.Map as Map
import System.IO
import Control.Monad
import Data.Monoid
import System.Environment
import Data.Word
import Data.Bits
import Numeric
import GHC.Int
import qualified Data.ByteString.Lazy.Char8 as Char8
import Data.Binary.Get as GetA
import Tags

data DicomTagString = Test | Test1 deriving (Show)
data TransferSyntax = ExplicitLE | ImplicitLE deriving (Eq)
type Group = Int64 
type Element = Int64 
data DicomTag = DicomString String | DicomGroupElement  {group :: Group, element :: Element}
type VR = String
type TagLength = Maybe Int64
data Value = ValInt Int64 | ValString String | ValDicomElem DicomElem | ValItems [DicomItem] deriving (Show) 
data DicomItem = DicomItem [DicomElem]
data DicomSeq = DicomSeq [DicomItem]
data DicomElem = DicomElem DicomTag VR TagLength Value
newtype DicomDataSet = DicomDataSet {getDicomDataSet ::  [DicomElem] }  

instance Show DicomTag where
    show (DicomGroupElement g e) = "0x" ++ (showHex g "") ++ " " ++ "0x" ++ (showHex e "") ++ " " ++  (tagNameFromElem g e)

instance Show DicomElem where 
    show (DicomElem t v tl (ValItems (x)) ) = show t ++ " " ++ show v ++ " " ++ show tl ++ " " ++ "\n" ++ show x ++ "\n"
    show (DicomElem t v tl val) = show t ++ " " ++ show v ++ " " ++ show tl ++ " " ++ show val ++ "\n"

instance Show DicomItem where 
    show (DicomItem []) = "" 
    --show (DicomItem (x:[])) = show x 
    show (DicomItem (x:xs)) = "----" ++ show x ++ show (DicomItem xs)

instance Show DicomSeq where
    show (DicomSeq (x:xs)) = "\n" ++ "----" ++ show x ++ "\n" ++ show (DicomSeq xs)

instance Show DicomDataSet where 
    show (DicomDataSet (x:xs)) = show x ++ "\n" ++ show (DicomDataSet xs)
    show (DicomDataSet []) = ""

instance Monoid DicomDataSet where 
    mempty = DicomDataSet []
    mappend (DicomDataSet a)  (DicomDataSet b) = DicomDataSet (a ++ b)

--Functions to read ByteStrings
snipBytes :: Int64 -> Int64 -> Char8.ByteString -> Char8.ByteString
snipBytes x y bs = (BS.take x (BS.drop y bs))

--File Meta Info
fileMetaInfo :: Char8.ByteString -> Char8.ByteString
fileMetaInfo bs = findTagBS bs metaTag ExplicitLE
    where metaTag = BS.pack [2,0,0,0]

readFileMeta :: Char8.ByteString -> Char8.ByteString
readFileMeta bs = snipBytes length 132 bs
    where length = fromIntegral(runGet read32le bsSnip) + 12
          bsSnip = snipBytes 4 8 $ fileMetaInfo $ BS.drop 132 bs

readMainBytes :: Char8.ByteString -> Char8.ByteString
readMainBytes bs = BS.drop (fromIntegral(132 + (BS.length $ readFileMeta bs)) :: Int64) bs

readFileMetaDataSet :: Char8.ByteString -> DicomDataSet 
readFileMetaDataSet bs = readDataSet ExplicitLE $ readFileMeta bs

readMainDataSet :: Char8.ByteString -> DicomDataSet 
readMainDataSet bs = readDataSet ExplicitLE $ readMainBytes bs

readItems :: Char8.ByteString -> TransferSyntax -> [DicomItem]
readItems bs ts 
    | bs == emptybs = [] 
    | otherwise = map (flip readItem ts) itemsBS
    where itemsBS = readItemsBS bs ts
          emptybs = Char8.pack ""
 
readItem :: Char8.ByteString -> TransferSyntax -> DicomItem
readItem bs ts 
    | bs == emptybs = DicomItem [] 
    | otherwise = DicomItem ( getDicomDataSet $ readDataSet ts bs)
    where emptybs = Char8.pack ""

readDataSet :: TransferSyntax -> Char8.ByteString -> DicomDataSet
readDataSet ts bs 
    | (elem == emptybs || bs == emptybs) = DicomDataSet []
    | otherwise = (DicomDataSet ([readDicomElem ts elem])) `mappend` (readDataSet ts lastBS)
        where vr = readVR ts bs
              numBytesLength = if (vr /= "OB" && vr /= "SQ" && vr /= "OW") then 2 else 6 
              numToDrop = if (ts == ExplicitLE) then 6 else 4 
              --lengthBytes = snipBytes numBytesLength numToDrop bs
              --length = readValueLength lengthBytes vr
              elem = readTag bs ts 
              lastBS = BS.drop ((fromIntegral  (BS.length elem)) :: Int64) bs 
              emptybs = Char8.pack ""

readValueLength :: Char8.ByteString -> VR -> TransferSyntax -> Maybe Int64
readValueLength bs vr ts
    | (vr /= "OB" && vr /= "SQ" && vr /= "OW" ) = Just $ (fromIntegral (runGet read16le $ BS.drop 6 bs) :: Int64)
    | bs /= Char8.pack "FFFFFFFF" = Just $ (fromIntegral (runGet read32le $  BS.drop 2 snipBS) :: Int64)
    | otherwise = Nothing 
    where snipBS = BS.drop 6 bs

--Handle if other byte
readDicomElem :: TransferSyntax -> Char8.ByteString -> DicomElem
readDicomElem ts bs 
    | ts == ExplicitLE = DicomElem (DicomGroupElement group element) vr tagLength value 
    where group = (fromIntegral(runGet getWord16le (BS.take 2 (BS.drop 0 bs))) :: Int64)
          element = (fromIntegral(runGet getWord16le (BS.take 2 (BS.drop 2 bs))) :: Int64)
          vr = readVR ts bs
          tagLength = readValueLength bs vr ts
          value = readValue bs vr ts
          --valBytes = readValue BS.drop 

--Need to make more generic to handle 16bit short values
readValue :: Char8.ByteString -> VR -> TransferSyntax -> Value
readValue bs vr ts 
    | vr == "IS" = ValString $ (Char8.unpack bs)
    | vr == "SQ" = ValItems $ readItems (BS.drop 12 bs) ts 
    | otherwise = ValString $ Char8.unpack $ BS.drop 8 bs 

readVR :: TransferSyntax -> Char8.ByteString -> VR 
readVR ts bs 
    | ts == ExplicitLE = Char8.unpack $ BS.take 2 $ BS.drop 4 bs

readTagIntS :: Char8.ByteString -> Int64 
readTagIntS bs = (fromIntegral(runGet getWord32le (BS.take 4 (BS.drop 8 bs))) :: Int64) 

readTagVL :: Char8.ByteString -> Char8.ByteString
readTagVL bs = snipBytes 2 6 bs

readTagVR :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTagVR bs ts = snipBytes 2 4 bs

readTagName :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTagName bs ts = snipBytes 4 0 bs

readTagOB :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTagOB bs ts = (BS.append) ((BS.append) ((BS.append)  (readTagName bs ts) (readTagVR bs ts) ) (readTagVLob bs)) (readValueBS bs length ts)
    where length = readOBLength bs ts 

readValueBS :: Char8.ByteString -> Int64 -> TransferSyntax -> Char8.ByteString
readValueBS bs x ts
    | ((readTagVR bs ts) /= Char8.pack "OB" || (readTagVR bs ts) /= Char8.pack "OW") =  snipBytes x 8 bs
    | otherwise = snipBytes x 12 bs

readTagVLob :: Char8.ByteString -> Char8.ByteString
readTagVLob bs = snipBytes 6 6 bs

--readOBLength :: Char8.ByteString -> Int64
--readOBLength bs = (fromIntegral(runGet read32le (snipBytes 4 8 bs)) :: Int64)
-- //////

readInt :: String -> Int
readInt = read

--hexMap :: Char8.ByteString -> ShowS
--hexMap x = Prelude.map ((flip showHex) "") (BS.unpack x)

readTags :: Char8.ByteString -> Char8.ByteString -> TransferSyntax -> [Char8.ByteString]
readTags bs tagbs ts 
           | otherwise = [readTag bs ts] ++ (readToTag (BS.drop (BS.length(readTag bs ts)) bs) tagbs ts)

readToTag :: Char8.ByteString -> Char8.ByteString -> TransferSyntax -> [Char8.ByteString]
readToTag bs tagbs ts 
    | ((readTagName bs ts) == tagbs) = [readTag bs ts] 
    | otherwise = [headTag] ++ (readToTag (BS.drop (BS.length(headTag)) bs) tagbs ts)
    where headTag = readTag bs ts

readToTagBS :: Char8.ByteString -> Char8.ByteString -> TransferSyntax -> Char8.ByteString
readToTagBS bs tagbs ts
    | bs == Char8.pack "" = Char8.pack ""
    | ((readTagName bs ts) == tagbs) = tag 
    | otherwise = BS.append tag (readToTagBS (BS.drop (BS.length tag) bs) tagbs ts)
    where tag = readTag bs ts

findTagBS :: Char8.ByteString -> Char8.ByteString -> TransferSyntax -> Char8.ByteString
findTagBS bs tagbs ts
    | bs == Char8.pack "" = Char8.pack ""
    | ((readTagName bs ts) == tagbs) = tag 
    | otherwise = (findTagBS (BS.drop (BS.length tag) bs) tagbs ts)
    where tag = readTag bs ts

readTagAll :: Char8.ByteString  -> TransferSyntax -> [Char8.ByteString]
readTagAll bs ts
    | (bs == BS.pack []) = [] 
    | (tag /= bs) = [tag] ++ readTagAll newBS ts 
    | otherwise = [tag]
    where len = BS.length tag
          tag = readTag bs ts
          newBS = BS.drop len bs 
            
readTag :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTag bs ts
    | (bs == Char8.pack "") = Char8.pack ""
    | ((readTagVR bs ts) == Char8.pack "OB" || (readTagVR bs ts) == Char8.pack "OW" ) = readTagOB bs ts
    | ((readTagVR bs ts) == Char8.pack "SQ") = readTagSeq bs ts 
    | (readTagName bs ts == BS.pack [254,255,0,224]) = readItemBS bs ts
    | (readTagName bs ts == BS.pack [254,255,13,224]) = readItemDelim bs ts
    | (readTagName bs ts == BS.pack [254,255,221,224]) = readSeqDelim bs ts
    | otherwise = readTagNorm bs ts

readTagNorm :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTagNorm bs ts = (BS.append) ((BS.append) ((BS.append)  (readTagName bs ts) (readTagVR bs ts) ) (readTagVL bs)) (readValueBS bs length ts)
         where length = readNormLength bs

--readTagOB :: Char8.ByteString -> Char8.ByteString
--readTagOB bs = (BS.append) ((BS.append) ((BS.append)  (readTagName bs) (readTagVR bs) ) (readTagVLob bs)) (readValue bs length)
--         where length = readOBLength bs 

readTagSeq :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readTagSeq bs ts -- readItem (BS.drop 12 bs) --readToTagBS (BS.drop 12 bs) seqDelim 
            | (tagLength == 4294967295) =  BS.append (BS.take 12 bs) (readToTagBS (BS.drop 12 bs) seqDelim ts)
            | otherwise = (BS.take (tagLength + 12) bs)
            where seqDelim = BS.pack [254,255,221,224] 
                  tagLength = (fromIntegral(runGet read32le (BS.take 4 (BS.drop 8 bs))) :: Int64)

readItemDelim :: Char8.ByteString ->  TransferSyntax  -> Char8.ByteString
readItemDelim bs ts = BS.take 8 bs

readSeqDelim :: Char8.ByteString ->  TransferSyntax -> Char8.ByteString
readSeqDelim bs ts = BS.take 8 bs

readItemBS :: Char8.ByteString -> TransferSyntax -> Char8.ByteString
readItemBS bs ts 
        | (tagLength == 4294967295) = BS.append (BS.take 8 bs) (readToTagBS (BS.drop 8 bs) itemDelim ts)
        | otherwise = (BS.take (8 + tagLength) bs)
        where itemDelim = BS.pack [254,255,13,224] 
              tagLength = (fromIntegral(runGet read32le (BS.take 4 (BS.drop 4 bs))) :: Int64)

readItemsBS :: Char8.ByteString ->  TransferSyntax -> [Char8.ByteString]
readItemsBS bs ts 
    | bs == Char8.pack "" = []
    | (readTagName bs ts == BS.pack [254,255,0,224]) = [headVal] ++ (readItemsBS (BS.drop (BS.length (headVal)) bs) ts)
    | (readTagName bs ts == BS.pack [254,255,13,224]) = []
    | otherwise = []  -- ++ (readItems ( BS.drop (BS.length (headVal) ) bs)) 
    where itemDelim = BS.pack [254,255,13,224]
          headVal =  readToTagBS (BS.drop 8 bs) itemDelim ts

readSQLength :: Char8.ByteString -> Int64
readSQLength bs = (fromIntegral(runGet read32le (snipBytes 4 8 bs)) :: Int64)

readNormLength :: Char8.ByteString -> Int64
readNormLength bs = (fromIntegral(runGet read16le bsSnip) :: Int64)
    where bsSnip = snipBytes 2 6 bs

readOBLength :: Char8.ByteString -> TransferSyntax -> Int64
readOBLength bs ts = (fromIntegral(runGet read32le bsSnip) :: Int64) 
    where bsSnip = snipBytes 4 8 bs
          drop = if ts == ExplicitLE then 8 else 6 


read16le :: Get (Word16)
read16le = do
    word <- getWord16le
    return word

read32le :: Get (Word32)
read32le = do
    word <- getWord32le
    return word

