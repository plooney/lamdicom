import System.IO  
import Control.Monad
import System.Environment
import Data.ByteString.Lazy as BS
import Data.Word
import Data.Bits
import Numeric
import GHC.Int
import Data.ByteString.Lazy.Char8 as Char8
IMPORT Data.Binary.Get as Get

main = do  
    args <- getArgs
    let list = []
    print (Prelude.head args)
    contents <- BS.readFile (Prelude.head args)
    print $ BS.take 34 ( BS.drop 128 contents) 
    print $ Prelude.map ((flip showHex) "") (BS.unpack (BS.take 34 (BS.drop 128 contents)))
    print $ fileMetaInfo contents
    print $ BS.unpack (readTagName (fileMetaInfo contents))
    print $ (readTagVR (fileMetaInfo contents))
    print $ BS.unpack (readTagVL (fileMetaInfo contents))
--    let vl = visualRepToValueLength (readTagVR (fileMetaInfo contents))
--    print $ vl
    let tagLength = runGet read16le (readTagVL (fileMetaInfo contents)) 
    print $ (fromIntegral(tagLength) :: Int64)
    --print $ runGet read32le (readValue (fileMetaInfo contents))
    let valueLength = (fromIntegral(tagLength) :: Int64) 
    let bs = BS.unpack (snipBytes valueLength 6 contents)
    print $ bs
    let tagTo = BS.pack [8,0,16,17]
    let tagTo = BS.pack [224,127,16,0]
    print $ BS.unpack tagTo
    print $ BS.unpack (readTag (snipBytes 300 132 contents))
    let tag = (readTag (snipBytes 300 132 contents))
    print $ BS.length tag
    print $ readNormLength tag
    --print $ BS.unpack (snipBytes 300 132 contents)
    print $ BS.unpack (readTag ((snipBytes 100 (132+ BS.length tag) contents)))
    print $ Prelude.map Char8.unpack (readToTag (BS.drop 132 contents) tagTo)
    --print $ (readToTag (snipBytes 800 132 contents) tagTo) 
    let tagLO = Prelude.last (readToTag (BS.drop 132 contents) tagTo)
    print $ readNormLength tagLO
    print $ Prelude.map readDicomElem (readToTag (BS.drop 132 contents) tagTo) 
    print $ Prelude.map readTagAll (readItems (BS.drop 12 (readTagSeq (snipBytes 200 766 contents))))
    --print $ readValue tagLO 18
    --print $ BS.unpack(readTagName tagLO)
    --print $ BS.length (readToTagBS (snipBytes 10000 132 contents) tagTo)
    --print $ (snipBytes valueLength 6) 

readInt :: String -> Int
readInt = read

read16le :: Get (Word16)
read16le = do
  word <- getWord16le
  return word

read32le :: Get (Word32)
read32le = do
  word <- getWord32le
  return word

--hexMap :: ByteString -> ShowS
--hexMap x = Prelude.map ((flip showHex) "") (BS.unpack x)

readTags :: ByteString -> ByteString -> [ByteString]
readTags bs tagbs  
           | otherwise = [readTag bs] ++ (readToTag (BS.drop (BS.length(readTag (bs))) bs) tagbs)

readToTag :: ByteString -> ByteString -> [ByteString]
readToTag bs tagbs  
           | ((readTagName bs) == tagbs) = [readTag bs] 
           | otherwise = [readTag bs] ++ (readToTag (BS.drop (BS.length(readTag (bs))) bs) tagbs)  

readToTagBS :: ByteString -> ByteString -> ByteString
readToTagBS bs tagbs
           | ((readTagName bs) == tagbs) = (readTag bs)
		   | otherwise = BS.append (readTag bs) (readToTagBS (BS.drop (BS.length(readTag (bs))) bs) tagbs)

readTagAll :: ByteString -> [ByteString]
readTagAll bs | (bs == BS.pack []) = [] 
readTagAll bs | (tag /= bs) = [tag] ++ readTagAll newBS 
              | otherwise = [tag]
           where len = BS.length tag
	         tag = readTag bs
	         newBS = BS.drop len bs 
            
readTag :: ByteString -> ByteString
readTag bs
         | ((readTagVR bs) == Char8.pack "OB") = readTagOB bs
         | ((readTagVR bs) == Char8.pack "SQ") = readTagSeq bs
	 | (readTagName bs == BS.pack [254,255,0,224]) = readItem bs
         | (readTagName bs == BS.pack [254,255,13,224]) = readItemDelim bs
         | (readTagName bs == BS.pack [254,255,221,224]) = readSeqDelim bs
         | otherwise = readTagNorm bs 

readTagNorm :: ByteString -> ByteString
readTagNorm bs = (BS.append) ((BS.append) ((BS.append)  (readTagName bs) (readTagVR bs) ) (readTagVL bs)) (readValue bs length)
         where length = readNormLength bs

readTagOB :: ByteString -> ByteString
readTagOB bs = (BS.append) ((BS.append) ((BS.append)  (readTagName bs) (readTagVR bs) ) (readTagVLob bs)) (readValue bs length)
         where length = readOBLength bs 

readTagSeq :: ByteString -> ByteString
readTagSeq bs -- readItem (BS.drop 12 bs) --readToTagBS (BS.drop 12 bs) seqDelim 
            | (tagLength == 4294967295) =  BS.append (BS.take 12 bs) (readToTagBS (BS.drop 12 bs) seqDelim)
            | otherwise = readToTagBS seqDelim (BS.drop 8 bs)  
            where seqDelim = BS.pack [254,255,221,224] 
                  tagLength = (fromIntegral(runGet read32le (BS.take 4 (BS.drop 8 bs))) :: Int64)

readItemDelim :: ByteString -> ByteString
readItemDelim bs = BS.take 8 bs

readSeqDelim :: ByteString -> ByteString
readSeqDelim bs = BS.take 8 bs

readItem :: ByteString -> ByteString
readItem bs 
        | (tagLength == 4294967295) = BS.append (BS.take 8 bs) (readToTagBS (BS.drop 8 bs) itemDelim)
        | otherwise = readToTagBS itemDelim (BS.drop 8 bs)  
        where itemDelim = BS.pack [254,255,13,224] 
              tagLength = (fromIntegral(runGet read32le (BS.take 4 (BS.drop 4 bs))) :: Int64)

readItems :: ByteString -> [ByteString]
readItems bs 
    | (readTagName bs == BS.pack [254,255,0,224]) = [headVal] ++ (readItems (BS.drop (BS.length (headVal)) bs))
    | (readTagName bs == BS.pack [254,255,13,224]) = []
    | otherwise = []  -- ++ (readItems ( BS.drop (BS.length (headVal) ) bs)) 
    where itemDelim = BS.pack [254,255,13,224]
          headVal =  readToTagBS (BS.drop 8 bs) itemDelim
--Length of Tag

readSQLength :: ByteString -> Int64
readSQLength bs = (fromIntegral(runGet read32le (snipBytes 4 8 bs)) :: Int64)

readNormLength :: ByteString -> Int64
readNormLength bs = (fromIntegral(runGet read16le (snipBytes 2 6 bs)) :: Int64)

readOBLength :: ByteString -> Int64
readOBLength bs = (fromIntegral(runGet read32le (snipBytes 4 8 bs)) :: Int64) 

readLength :: ByteString -> Int64
readLength bs
          | ((readTagVR bs) /= Char8.pack "OB") = readOBLength bs 
	  | otherwise = readNormLength bs 

readTagName :: ByteString -> ByteString
readTagName bs = snipBytes 4 0 bs

readTagVR :: ByteString -> ByteString
readTagVR bs = snipBytes 2 4 bs

readTagVLob :: ByteString -> ByteString
readTagVLob bs = snipBytes 6 6 bs

readTagVL :: ByteString -> ByteString
readTagVL bs = snipBytes 2 6 bs

readValue :: ByteString -> Int64 -> ByteString
readValue bs x 
        | ((readTagVR bs) /= Char8.pack "OB") =  snipBytes x 8 bs
        | otherwise = snipBytes x 12 bs

snipBytes :: Int64 -> Int64 -> ByteString -> ByteString
snipBytes x y bs = (BS.take x (BS.drop y bs)) 

fileMetaInfo :: ByteString -> ByteString 
fileMetaInfo bs = snipBytes 12 132 bs 

--visualRepToValueLength :: ByteString -> Int64
--visualRepToValueLength bs
--                     | bs ==  (Char8.pack "UL") = 10
    
--Dicom Elem
bsToDicomElem :: [ByteString] -> [DicomElem]
bsToDicomElem b = Prelude.map readDicomElem b 

readSeqElem :: ByteString -> [[DicomElem]]
--readSeqElem bs = (readItems (readToTagBS (BS.drop 12 bs) seqDelim))
readSeqElem bs = Prelude.map  bsToDicomElem  (Prelude.map readTagAll (readItems (readToTagBS (BS.drop 12 bs) seqDelim))) --bsToDicomElem (readToTag (BS.drop 12 bs) seqDelim)
            where seqDelim = BS.pack [254,255,221,224]

readItemElem :: ByteString -> [DicomElem]
readItemElem bs  
             | (tagLength == 4294967295) = bsToDicomElem [bs]---(readToTag (BS.drop 8 bs) itemDelim)
             | otherwise = bsToDicomElem (readToTag itemDelim (BS.drop 8 bs))
             where itemDelim = BS.pack [254,255,13,224]
                   tagLength = (fromIntegral(runGet read32le (snipBytes 4 4 bs)) :: Int64)

readDicomElem :: ByteString -> DicomElem 
readDicomElem bs 
          | ((readTagVR bs) == Char8.pack "OB") = DicomElemString {tag= BS.unpack (readTagName bs),valueString = (readValue bs (readOBLength bs))} 
          | ((readTagVR bs) == Char8.pack "SQ") = DicomElemSQ {tag=BS.unpack (readTagName bs),valueSQ = readSeqElem bs} 
          | (readTagName bs == BS.pack [254,255,0,224]) = DicomElemString {tag=BS.unpack (readTagName bs),valueString =  BS.pack []} 
          | (readTagName bs == BS.pack [254,255,13,224]) = DicomElemString {tag=BS.unpack (readTagName bs),valueString =  BS.pack []} 
          | (readTagName bs == BS.pack [254,255,221,224]) = DicomElemString {tag=BS.unpack (readTagName bs),valueString =  BS.pack []} 
          | otherwise =  DicomElemString {tag=BS.unpack (readTagName bs),valueString =  (readValue bs (readNormLength bs))} 

data DicomElem = DicomElemString {tag :: [Word8], valueString :: ByteString} | DicomElemInt {tag :: [Word8], valueInt :: Int} | DicomElemSQ {tag :: [Word8], valueSQ :: [[DicomElem]]} deriving Show    
