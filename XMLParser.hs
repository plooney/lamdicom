module XMLParser(
readTagTuples,
readRows
) where

import Text.XML.Light
import Text.XML.Light.Cursor
import Data.ByteString.Lazy as BS
import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellStyle)

type Filepath = String

getParas :: ByteString -> [Element]
getParas bs = elChildren $ Prelude.head $ Prelude.drop 1 y 
    where y = (onlyElems . parseXML) bs  

getInfoTable :: [Element] -> [Element]
getInfoTable els = elChildren $ Prelude.head $ elChildren $ Prelude.head $ Prelude.drop 3 els 

readRows :: ByteString -> [Element]
readRows = elChildren . Prelude.head . getInfoTable . getParas

readEntry :: Int -> Element -> String
readEntry x = strContent . Prelude.head . onlyElems . elContent . Prelude.head .(Prelude.drop x) . elChildren  

readTagTuples :: [Element] -> [(String,String,String,String)]
readTagTuples els = Prelude.foldl (\acc x -> acc ++ (tup x)) [] els --[("","",""),("","","")] 
    where tagElem = Prelude.take 4 . Prelude.drop 1 . readEntry 0 
          tagGroup = Prelude.take 4 . Prelude.drop 6 . readEntry 0 
          tagName = readEntry 2  
          vr = readEntry 3 
          tup = \x -> [(tagElem x, tagGroup x, tagName x, vr x)]


