import Data.ByteString.Lazy as BS
import Data.List as LS
import DicomData
import XMLParser
import Numeric

writeImports :: FilePath -> [String] -> IO()
writeImports fp ss = Prelude.appendFile fp $ LS.concat $ LS.map (\x -> "import " ++ x ++ "\n") ss

--writeTagNameFromElemGroup :: FilePath -> [String] -> IO()
--writeTagNameFromElemGroup fp ss = Prelude.appendFile fp $ "data Tag = " ++ LS.concat tagsIS ++ " deriving Show" 
--    where tagsIS = LS.intersperse " | " ss

writeTagNameFromElemGroup :: FilePath -> [(String,String,String,String)] -> IO()
writeTagNameFromElemGroup fp tups = init >> Prelude.appendFile fp ( Prelude.drop 0 tags )
    where init = Prelude.appendFile fp "\ntagNameFromElem :: Element -> Group -> String\ntagNameFromElem e g\n" 
          tags = LS.concat $ Prelude.map (\tup -> "    | " ++ (writeTup tup) ++ "\n") filTups 
          hexInt x = show . readHex $ x  
          filTups = LS.filter (\(w,x,y,z) -> Prelude.length w == 4 && Prelude.length x ==4 ) tups 
          
writeTup :: (String,String,String,String) -> String
writeTup (w, ("xxxx"),y,z) = "e == " ++ w ++ " = \"" ++ y ++ "\""
writeTup (w, (_:_:'x':xs),y,z) = "e == " ++ w ++ " && mod (g -" ++  (show . hexInt $ xs) ++ ") 10 == 0 = \"" ++ y  ++ "\""
writeTup (w,x,y,z) = "e == " ++ (show . hexInt $ w) ++ " && g == " ++ (show . hexInt $ x)  ++ " = \"" ++ y ++ "\""

hexInt = fst . Prelude.head . readHex

--writeElemGroupToString :: FilePath -> IO()
--writeElemGroupToString fp = Prelude.appendFile fp "\ntagName :: Element -> Group -> (String,String)\n" >>  
--    where whereLine = Prelude.appendFile fp "tagName e g = ()"

-- elemGroupToInt :: Elem -> Group -> Int
-- fst . Prelude.head . readHex $ "00000010"

--lookup :: ByteString -> Tag
--lookup bs = Test

--writeBysteStringToTag  
--
--
--



main :: IO()
main = Prelude.writeFile file [] 
       -- >> Prelude.appendFile file "type Elem = Int"
       -- >> Prelude.appendFile file "\n" 
       -- >> Prelude.appendFile file "type Group = Int"
       >> writeImports file ["DicomData"]
       >> BS.readFile "xml/part6.xml"
       >>= writeTagNameFromElemGroup "Tags.hs" . Prelude.drop 1 . readTagTuples . readRows   
    where file = "Tags.hs"
 --      >> writeTags "Tags.hs" ["aaa","sss"] 


-- data Elem [String]
-- data Group [String]
-- data VR [String]
-- tagNameFromElem :: Elem -> Group -> String
-- String -> (Elem,Group)
-- Elem -> Group -> VR
